/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.guesstheword.screens.score

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.android.guesstheword.R
import com.example.android.guesstheword.databinding.ScoreFragmentBinding

/**
 * Fragment where the final score is shown, after the game is over
 */
class ScoreFragment : Fragment() {

    private lateinit var viewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        // Inflate view and obtain an instance of the binding class.
        val binding: ScoreFragmentBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.score_fragment,
                container,
                false
        )

        // creating a view model factory and passing it to the view model allows the view model use
        // data that was passed to the fragment from navigation when the view model is created instead
        // of waiting for a getter in the fragment to pass the data to the view model
        viewModelFactory = ScoreViewModelFactory(ScoreFragmentArgs.fromBundle(arguments!!).score)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)

        // make the view model available to the layout without having to go through the UI controller
        binding.scoreViewModel = viewModel
        // make the liveData available to the layout without having to go through the UI controller
        binding.lifecycleOwner = this

        // observables for score and play again
        // since the layout has access to the view model we can update the layout when the data changes and the observers are no longer needed here
//        viewModel.score.observe(viewLifecycleOwner, Observer { newScore -> binding.scoreText.text = newScore.toString() })
        viewModel.playAgain.observe(viewLifecycleOwner, Observer { playAgain -> if (playAgain) findNavController().navigate(ScoreFragmentDirections.actionRestart()) })

        // click listener for the play again button
        // since the layout has access to the view model we can set this click listeners in the layout xml and it is no longer needed here
//        binding.playAgainButton.setOnClickListener { viewModel.playAgain() }

        return binding.root
    }
}
